# Naming Conventions #

### This page shows examples and guidelines for naming conventions in projects. ###

All conventions show a pattern for naming and examples of that pattern. All use either [`camelCase`](https://en.wikipedia.org/wiki/Camel_case) or [`snake_case`](https://en.wikipedia.org/wiki/Snake_case). Follow the links to know more about them.
As a rule of thumb be as descriptive as you can when naming anything while also using short names.


## View Ids and Names: ##
**Pattern:**
First two letters of a name must show the class of that view. Then the rest of the name should be a couple of words to clearly show the purpose of that view.

**Case:**
`camelCase`

**Example1:**
A checkbox is used to get user's agreement about terms and conditions.
First two letters must be "cb" an abbreviation of checkbox.
The rest of the name must be something like "agreed" which shows the boolean nature of the view or "terms" which shows what this view is being used for. Or it can be a combination of the two like "termsAgreement".
So our view should be named either like "cbAgreed", "cbTerms" or even better "cbTermsAgreement".

**Example2:**
An EditText is used to get user's address. First two letters should be "et" an abbreviation of EditText. Second part of the name can be "Address". So the full name ends up being "etAddress".

**Example3:**
A ViewPager view is often used only once per activity or fragment. Views like these don't need to be named like above and can be simply called "viewPager". The important point is avoid confusion. 
In case of multiple views of same type, confusion can be avoided by naming them distinctly and descriptively. But in case of a one of a kind view like ViewPager or DrawerLayout, that is not neccessary.

## Activities and Fragments: ##

**Pattern:**
All activity and fragment names must start with one or two words clearly explaining their purpose. All activity names must end with "Activity" and all fragment names must end with "Fragment". All class names start with Capitol letters in java.

**Case:**
`camelCase`

**Example1:**
An activity used for creating a user account can be called "RegisterActivity" or "SignUpActivity".


## Layout Resources:##

**Pattern:**
All layout must start with the word explaining what type of layout they are. An activity layout starts with "activity", fragment layout with "fragment" and a dialog layout with "dialog" and a list item layout with "layout_item". The second half of the name contains the name of the java class they are associated with but in snake_case form.

**Case:**
`snake_case`

**Example1:**
A layout associated with LoginFragment.java must be called "fragment_login".

**Example2:**
An item layout associated with a ListView in ProductsActivity.java can be called "layout_item_products"



